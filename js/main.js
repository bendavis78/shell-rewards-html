// Tabs
$.fn.tabs = function() {
  var $container = $(this);
  $container.find(".tabs-menu a").click(function(e) {
    var $a = $(this);
    e.preventDefault();
    $a.parent().addClass("active");
    $a.parent().siblings().removeClass("active");
    var tab = $a.attr("href");
    $container.find(".tab-content").not(tab).css("display", "none");
    $(tab).fadeIn();
  });
};

$.fn.accordian = function() {
  var $container = $(this);
  var $all = $container.find('.heading + .detail');
  $all.hide();
  $container.find('.heading').click(function() { 
    $all.slideUp();
    $(this).next().not(':visible').slideDown();
  });
};

$(document).ready(function() {

	//Open links in new window
	$("a[rel=external]").each(function(){
		this.target = "_blank";
	});

	//default validate action
	$('form').attr('novalidate',true);

	$("a.print").click(function(){
		window.print();
		return false;
	});
});
